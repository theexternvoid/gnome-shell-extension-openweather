const Gettext = imports.gettext.domain('gnome-shell-extension-openweather');
const _ = Gettext.gettext;

var UseCurrentLocation = "~~~CURRENT_LOCATION~~~";

function isCurrentLocationSelection(locationProviderPref) {
	return locationProviderPref.split(">")[1] == UseCurrentLocation;
}

function extractLocation(locationProviderPref) {
	if (locationProviderPref == null)
		return "";

	if (locationProviderPref.search(">") == -1)
		return _("Invalid city");
	let location = locationProviderPref.split(">")[1];
	if (isCurrentLocationSelection(locationProviderPref))
		return _("Current location");
	return location;
}

function extractCoord(locationProviderPref) {
	let coords = "0";
	if (locationProviderPref != null && (locationProviderPref.search(">") != -1))
		coords = locationProviderPref.split(">")[0].replace(' ', '');
	if (!isCurrentLocationSelection(locationProviderPref) && (coords.search(",") == -1 || isNaN(coords.split(",")[0]) || isNaN(coords.split(",")[1])))
		coords = "0";
	return coords;
}

function extractProvider(locationProviderPref) {
	if (!locationProviderPref)
		return -1;
	if (locationProviderPref.split(">")[2] === undefined)
		return -1;
	if (isNaN(parseInt(locationProviderPref.split(">")[2])))
		return -1;
	return parseInt(locationProviderPref.split(">")[2]);
}
